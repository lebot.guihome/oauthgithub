<?php

namespace App\Security;

use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;

class NotVerifyEmailExeption extends CustomUserMessageAuthenticationException
{
    public function __construct(
        $message = "Ce compte ne possede pas d'email vérifié",
        $messageArray = [],
        $code = 0,
        $previous = null
        ){
            parent::__construct($message, $messageArray, $code, $previous);
    }
}