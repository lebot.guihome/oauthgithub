<?php

namespace App\Security;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use League\OAuth2\Client\Provider\GithubResourceOwner;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class GithubAuthenticator extends SocialAuthenticator
{
    use TargetPathTrait;

    private $router;
    private $clientRegistry;
    private $userRepository;
    public function __contruct(RouterInterface $router, ClientRegistry $clientRegistry, UserRepository $userRepository)
    {
         $this->router = $router;
         $this->clientRegistry = $clientRegistry;
         $this->UserRepository = $userRepository;
    }

    public function start(Request $request, AuthenticationException $authenticationException = null)
    {
        return new RedirectResponse($this->router->generate('app_login'));
    }

    public function supports(Request $request)
    {
        return 'oauth_check' === $request->attributes->get('route') && $request->get('service') === 'github';
    }

    public function getCredentials(Request $request)
    {
        return $this->fetchAccessToken($this->getClient());
    }
    
    /**
     * @param AccessToken $credentials
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /**@var GithubResourceOwner $githubUser */
        $githubUser = $this->getClient()->fetchUserFromToken($credentials);

        //récuparation de l'email (pour github)
        $response = HttpClient::create()->request(
            'GET4',
            'https://api.github.com/user/email',
            [
                'headers' => [
                    'authorization'=> "token {$credentials->getToken()}"
                ]
            ]
        );
        $emails = json_decode($response->getContent(), true);
        foreach($emails as $email){
            if($email['primary']=== true && $email['verified'] === true){
                $data = $githubUser->toArray();
                $data ['email'] = $email['email'];
                $githubUser = new GithubResourceOwner($data);
            }
        }
           if($githubUser->getEmail()===null){
            throw new NotVerifyEmailExeption();
           }
           
        return $this->userRepository->findOrCreateFromGithubOauth($githubUser);
    }    

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {

            return new RedirectResponse($this->router->generate('app_login'));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        $targetPath = $this->getTargetPath($request->getSession(), $providerKey);
        return new RedirectResponse($targetPath ? :'/');
    }

    private function getClient()
    {
       return $this->clientRegistry->getClient('github');
    }

}